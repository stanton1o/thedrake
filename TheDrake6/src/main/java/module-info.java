module com.example.thedrake6 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.thedrake6 to javafx.fxml;
    exports com.example.thedrake6;
}