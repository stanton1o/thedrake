package com.example.thedrake6;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Application extends javafx.application.Application {

    static private boolean fullScreen;
    @Override
    public void start(Stage stage) throws IOException {

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("drake.fxml")));
        Scene scene = new Scene(root);
        stage.setTitle("The Drake");
        stage.setScene(scene);
        stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("images/icon.png"))));

        fullScreen = true;
        stage.setFullScreen(fullScreen);
        stage.show();

        scene.setOnKeyPressed(keyEvent ->  {
            if(keyEvent.getCode() == KeyCode.ESCAPE){
                System.out.println("FullScreen: " + !fullScreen);
                fullScreen = !fullScreen;
                stage.setFullScreen(fullScreen);
            }
        });

    }

    public static void main(String[] args) {
        launch();
    }
}