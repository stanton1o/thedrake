package thedrake;

import java.io.PrintWriter;

public class Board {

	final private BoardTile[][] gameBoard;
	final private int dimension;

	// Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
	public Board(int dimension) {
		this.dimension = dimension;
		gameBoard = new BoardTile[dimension][dimension];
		for ( int i = 0 ; i < dimension ; i++)
			for ( int j = 0 ; j < dimension ; j++)
				gameBoard[i][j] = BoardTile.EMPTY;
	}

	// Rozměr hrací desky
	public int dimension() {
		return dimension;
	}

	public BoardTile[][] getGameBoard() {
		return gameBoard;
	}

	// Vrací dlaždici na zvolené pozici.
	public BoardTile at(TilePos pos) {
		return  gameBoard[pos.i()][pos.j()];
	}

	// Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
	public Board withTiles(TileAt ats,TileAt tileAt, TileAt at) {
		Board x = new Board(dimension);
		for ( int i = 0 ; i < dimension ; i++){
			//x.gameBoard = gameBoard.clone();
			for ( int j = 0 ; j < dimension ; j++){
				x.gameBoard[i][j] = gameBoard[i][j];

				if (i == ats.pos.i() && j == ats.pos.j())
					x.gameBoard[i][j] = ats.tile;
				if (i == tileAt.pos.i() && j == tileAt.pos.j())
					x.gameBoard[i][j] = tileAt.tile;
				if (i == at.pos.i() && j == at.pos.j())
					x.gameBoard[i][j] = at.tile;
			}
		}

		return x;
	}

	public Board withTiles(TileAt ats,TileAt tileAt) {
		Board x = new Board(dimension);
		for ( int i = 0 ; i < dimension ; i++){
			//x.gameBoard = gameBoard.clone();
			for ( int j = 0 ; j < dimension ; j++){
				x.gameBoard[i][j] = gameBoard[i][j];

				if (i == ats.pos.i() && j == ats.pos.j())
					x.gameBoard[i][j] = ats.tile;
				if (i == tileAt.pos.i() && j == tileAt.pos.j())
					x.gameBoard[i][j] = tileAt.tile;
			}
		}

		return x;
	}

	public Board withTiles(TileAt a3) {
		Board x = new Board(dimension);
		for ( int i = 0 ; i < dimension ; i++){
			for ( int j = 0 ; j < dimension ; j++){
				x.gameBoard[i][j] = gameBoard[i][j];

				if (i == a3.pos.i() && j == a3.pos.j())
					x.gameBoard[i][j] = a3.tile;
			}
		}

		return x;
	}

	// Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
	public PositionFactory positionFactory() {
		 return new PositionFactory(dimension);
	}

    public void toJSON(PrintWriter writer) {
		writer.print("{\"dimension\":"+dimension+",\"tiles\":[");
		for (int i = 0; i < dimension; i++)
		{
			for (int j = 0; j < dimension; j++)
			{
				gameBoard[j][i].toJSON(writer);
				if (i == dimension - 1 && j == dimension - 1)
					break;
				else
					writer.print(",");
			}
		}
		writer.print("]}");
	}
    public static class TileAt {
		public final BoardPos pos;
		public final BoardTile tile;
		
		public TileAt(BoardPos pos, BoardTile tile) {
			this.pos = pos;
			this.tile = tile;
		}
	}
}

