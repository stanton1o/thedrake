package thedrake;

import java.io.PrintWriter;
import java.util.*;

public class BoardTroops implements JSONSerializable{
	private final PlayingSide playingSide;
	private final Map<BoardPos, TroopTile> troopMap ;
	private final TilePos leaderPosition;
	private final int guards;

	public BoardTroops(PlayingSide playingSide) {
		this.playingSide = playingSide;
		leaderPosition = TilePos.OFF_BOARD;
		troopMap = new HashMap<>();
		//troopMap = Collections.emptyMap(); --> map is immutable <--
		guards = 0;
	}

	public BoardTroops(
			PlayingSide playingSide,
			Map<BoardPos, TroopTile> troopMap,
			TilePos leaderPosition, 
			int guards) {
		this.playingSide = playingSide;
		this.leaderPosition = leaderPosition;
		this.guards = guards;
		this.troopMap = troopMap;
	}

	public Optional<TroopTile> at(TilePos pos) {
		if (troopMap.containsKey((BoardPos)pos))
			return Optional.of(troopMap.get((BoardPos)pos));
		else return Optional.empty();
	}

	public Map<BoardPos, TroopTile> getTroopMap() {
		return troopMap;
	}

	public PlayingSide playingSide() {
		return playingSide;
	}
	
	public TilePos leaderPosition() {
			return leaderPosition;
	}

	public int guards() {
		return guards;
	}
	
	public boolean isLeaderPlaced() {
		return leaderPosition != TilePos.OFF_BOARD;
	}
	
	public boolean isPlacingGuards() {
		if (this.isLeaderPlaced())
			return guards < 2;
		else
			return false;
	}	
	
	public Set<BoardPos> troopPositions() {
		return new HashSet<>(troopMap.keySet());
	}

/*	Vrací novou instanci BoardTroops s novou dlaždicí TroopTile na pozici target obsahující jednotku troop lícovou stranou nahoru.
Tato metoda vyhazuje výjimku IllegalArgumentException, pokud je již zadaná pozice obsazena jinou dlaždicí.
Pokud pomocí této metody stavíme úplně první jednotku, bere se tato jednotka jako vůdce a je potřeba nastavit pozici leaderPosition.
Tímto hra přechází do fáze stavění stráží.
Pokud pomocí této metody stavíme druhou a třetí jednotku, jsme ve fázi stavění stráží. Tato fáze konči ve chvíli, kdy jsou obě stráže postaveny.
 */
	public BoardTroops placeTroop(Troop troop, BoardPos target) {
		if ( troopMap.containsKey(target)) {
			throw new IllegalArgumentException();
		}
		Map<BoardPos, TroopTile>map = new HashMap<>(troopMap);
		map.put(target,new TroopTile(troop,playingSide,TroopFace.AVERS));
		if ( ! this.isLeaderPlaced())
			return  new BoardTroops(playingSide, map, target, guards );

		else if (this.isPlacingGuards()) {
			return  new BoardTroops(playingSide, map, leaderPosition, guards+1 );
		}
		else
			return  new BoardTroops(playingSide, map, leaderPosition, guards );
}



	//Vrací novou instanci BoardTroops s dlaždicí TroopTile na pozici origin posunutou na pozici
	// target a otočenou na opačnou stranu. Tato metoda vyhazuje výjimku IllegalStateException
	// pokud jsme ve stavu stavění vůdce, nebo stavění stráží, neboť v těchto fázích nelze ješte s
	// dlaždicemi pohybovat. Metoda dále vyhazuje výjimku IllegalArgumentException pokud je pozice origin prázdná nebo pozice target již obsazená.
	//Pozor, že pokud pomocí této metody pohybujeme s vůdce, je třeba aktualizovat jeho pozici.
	public BoardTroops troopStep(BoardPos origin, BoardPos target) {

		if (! this.isLeaderPlaced() || this.isPlacingGuards() )
			throw new IllegalStateException();

		if ( troopMap.containsKey(target) || !troopMap.containsKey(origin)) {
			throw new IllegalArgumentException();
		}

		troopMap.put(target,troopMap.get(origin).flipped());
		troopMap.remove(origin);

		if (origin.equals(leaderPosition))
			return new BoardTroops(playingSide, troopMap, target, guards );
		else
			return new BoardTroops(playingSide, troopMap, leaderPosition, guards );
	}

	public BoardTroops troopFlip(BoardPos origin) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");			
		}
		
		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");			
		}
		
		if(at(origin).isEmpty())
			throw new IllegalArgumentException();
		
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);
		newTroops.put(origin, tile.flipped());

		return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
	}
	//vrací novou instanci BoardTroops s odstraněnou dlaždicí z pozice target
	// Podobně jako metoda troopStep vyhazuje výjimku IllegalStateException,
	// pokud jsme ve stavu stavění vůdce, nebo stavění střáží. Dále vyhazuje výjimku IllegalArgumentException pokud je pozice target prázdná.
	//Pozor, pokud pomocí této metody odstraňujeme vůdce, je třeba jeho pozici nastavit zpět na TilePos.OFF_BOARD.
	public BoardTroops removeTroop(BoardPos target) {
		if (! this.isLeaderPlaced() || this.isPlacingGuards() )
			throw new IllegalStateException();

		if (! troopMap.containsKey(target)) {
			throw new IllegalArgumentException();
		}
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.remove(target);
		if (Objects.equals(target, leaderPosition)){
			return new BoardTroops(playingSide, newTroops, TilePos.OFF_BOARD, guards);
		}
		else
			return new BoardTroops(playingSide, newTroops, leaderPosition, guards);
	}

		public void toJSON(PrintWriter writer) {
			Map<BoardPos, TroopTile> sortedTroopMap = new TreeMap<>(new Comparator<>() {
				@Override
				public int compare(BoardPos o1, BoardPos o2) {
					return o1.toString().compareTo(o2.toString());
				}
			});

			sortedTroopMap.putAll(troopMap);

			int size = sortedTroopMap.size(), cnt = 0;

			writer.print("{\"side\":\"" + playingSide + "\",\"leaderPosition\":\"" + leaderPosition + "\",\"guards\":" + guards + ",\"troopMap\":{");

			for(Map.Entry<BoardPos, TroopTile> i : sortedTroopMap.entrySet()){
				++cnt;
				writer.print("\"" + i.getKey() + "\":");
				if(cnt == size)
					writer.print("{\"troop\":\""+ i.getValue().troop().name()+"\",\"side\":\""+i.getValue().side()+"\",\"face\":\""+i.getValue().face()+"\"}}");
				else
					writer.print("{\"troop\":\""+ i.getValue().troop().name()+"\",\"side\":\""+i.getValue().side()+"\",\"face\":\""+i.getValue().face()+"\"},");
			}
			writer.print("}");
	}
}
