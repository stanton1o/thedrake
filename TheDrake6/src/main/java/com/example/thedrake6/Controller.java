package com.example.thedrake6;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Controller {
    @FXML
    private FlowPane paneid;
    @FXML
    private Button exitbtn;

    static private Stage stage1;
    static private Stage stage2;
    static private Button btn;

    public void showExitMenu() throws IOException {
        stage1 = (Stage) paneid.getScene().getWindow();
        stage2 = new Stage();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("exit.fxml")));
        stage2.setScene(new Scene(root));
        stage2.setTitle("The Drake exit");
        stage2.setResizable(false);
        stage2.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("images/icon.png"))));
        stage2.show();

        btn = exitbtn;
        btn.setDisable(true);
        stage2.setOnCloseRequest(we -> {
            btn.setDisable(false);
            System.out.println("The exit menu has been closed");
        });
        stage1.setOnCloseRequest(we -> {
            System.out.println("The program has been completed");
            stage2.close();
        });
    }

    public void closeProgram() {
        System.out.println("The program has been completed");
        stage2.close();
        stage1.close();
    }

    public void closeExitMenu() {
        System.out.println("The exit menu has been closed");
        btn.setDisable(false);
        stage2.close();
    }


}