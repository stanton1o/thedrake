package thedrake;

import java.io.PrintWriter;
import java.util.*;

public class Army implements JSONSerializable{
	private final BoardTroops boardTroops;
	private final List<Troop> stack;
	private final List<Troop> captured;
	
	public Army(PlayingSide playingSide, List<Troop> stack) {
		this(
				new BoardTroops(playingSide), 
				stack, 
				Collections.emptyList());
	}
	
	public Army( 
			BoardTroops boardTroops,
			List<Troop> stack, 
			List<Troop> captured) {
		this.boardTroops = boardTroops;
		this.stack = stack;
		this.captured = captured;
	}
	
	public PlayingSide side() {
		return boardTroops.playingSide();
	}
	
	public BoardTroops boardTroops() {
		return boardTroops;
	}
	
	public List<Troop> stack() {
		return stack;
	}
	
	public List<Troop> captured() {
		return captured;
	}

	public Army placeFromStack(BoardPos target) {
		if(target == TilePos.OFF_BOARD) 
			throw new IllegalArgumentException();
		
		if(stack.isEmpty())
			throw new IllegalStateException();
		
		if(boardTroops.at(target).isPresent())
			throw new IllegalStateException();

		List<Troop> newStack = new ArrayList<Troop>(
				stack.subList(1, stack.size()));
		
		return new Army(
				boardTroops.placeTroop(stack.get(0), target),
				newStack, 
				captured);
	}
	
	public Army troopStep(BoardPos origin, BoardPos target) {
		return new Army(boardTroops.troopStep(origin, target), stack, captured);
	}
	
	public Army troopFlip(BoardPos origin) {
		return new Army(boardTroops.troopFlip(origin), stack, captured);
	}
	
	public Army removeTroop(BoardPos target) {
		return new Army(boardTroops.removeTroop(target), stack, captured);
	}
	
	public Army capture(Troop troop) {
		List<Troop> newCaptured = new ArrayList<Troop>(captured);
		newCaptured.add(troop);
		
		return new Army(boardTroops, stack, newCaptured);
	}

    public void toJSON(PrintWriter writer) {
		Map<BoardPos, TroopTile> sortedTroopMap = new TreeMap<>(new Comparator<>() {
			@Override
			public int compare(BoardPos o1, BoardPos o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});

//		"{\"boardTroops\":{\"side\":\"ORANGE\",\"leaderPosition\":\"a4\",\"guards\":2,\"troopMap\":" +
//				"{\"a3\":{\"troop\":\"Clubman\",\"side\":\"ORANGE\",\"face\":\"AVERS\"},\"a4\":" +
//				"{\"troop\":\"Drake\",\"side\":\"ORANGE\",\"face\":\"AVERS\"},\"b4\":" +
//				"{\"troop\":\"Clubman\",\"side\":\"ORANGE\",\"face\":\"AVERS\"}}},\"stack\":" +
//				"[\"Monk\",\"Spearman\",\"Swordsman\",\"Archer\"],\"captured\":[]}";

				sortedTroopMap.putAll(boardTroops.getTroopMap());

		int size = sortedTroopMap.size(), cnt = 0;

		writer.print("{\"boardTroops\":{\"side\":\"" +
				boardTroops.playingSide() + "\",\"leaderPosition\":\"" +
				boardTroops.leaderPosition() + "\",\"guards\":" +
				boardTroops.guards() + ",\"troopMap\":{");

		if (size == 0)
			writer.print("}");
		for(Map.Entry<BoardPos, TroopTile> i : sortedTroopMap.entrySet()){
			++cnt;
			writer.print("\"" + i.getKey() + "\":");
			writer.print("{\"troop\":\""+ i.getValue().troop().name()+"\",\"side\":\""+i.getValue().side()+"\",\"face\":\""+i.getValue().face()+"\"}");
			if(cnt == size)
				writer.print("}");
			else
				writer.print(",");
		}
		writer.print("},\"stack\":[");

		cnt = 0;
		size = stack.size();
		for (Troop i : stack){
			++cnt;
			if(cnt != size)
				writer.print("\""+ i.name() +"\",");
			else
				writer.print("\""+ i.name() +"\"");
		}
		writer.print("]");
		cnt = 0;
		size = captured.size();
		writer.print(",\"captured\":[");
		for (Troop i : captured){
			++cnt;
			if(cnt != size)
				writer.print("\""+ i.name() +"\",");
			else
				writer.print("\""+ i.name() +"\"");
		}
		writer.print("]}");
    }
}
