package thedrake;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile, JSONSerializable{

    private final Troop troop;
    private final PlayingSide side;
    private final TroopFace face;

    // Konstruktor
    public TroopTile(Troop troop, PlayingSide side, TroopFace face){
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    // Vrací barvu, za kterou hraje jednotka na této dlaždici
    public PlayingSide side(){
        return side;
    }

    // Vrací stranu, na kterou je jednotka otočena
    public TroopFace face(){
        return face;
    }

    // Jednotka, která stojí na této dlaždici
    public Troop troop(){
        return troop;
    }

    // Vrací False, protože na dlaždici s jednotkou se nedá vstoupit
    @Override
    public boolean canStepOn(){
        return false;
    }

    // Vrací True
    @Override
    public boolean hasTroop(){
        return true;
    }

    // Upravte vaši třídu TroopTile tak,
    // aby implementovala metodu movesFrom tak,
    // že projde všechny akce na správné straně jednotky a sjednotí jimi vrácené tahy do jednoho seznamu.
    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> allAction = new ArrayList<>();
        List<TroopAction> action = troop().actions(face());

        for (TroopAction i: action)
            allAction.addAll(i.movesFrom(pos, side(), state));

        return allAction;
    }

    // Vytvoří novou dlaždici, s jednotkou otočenou na opačnou stranu
// (z rubu na líc nebo z líce na rub)
    public TroopTile flipped(){
        if ( face == TroopFace.AVERS)
            return new TroopTile(troop, side, TroopFace.REVERS);
        else
            return new TroopTile(troop, side, TroopFace.AVERS);

    }

    public void toJSON(PrintWriter writer) {
        writer.print("{\"troop\":\"" + troop.name() + "\",\"side\":\"" + side + "\",\"face\":\"" + face + "\"}");
    }
}
